import GameManager, { GMEvent } from "./GameManager";
import State from "./StateMachine/State";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ScoreManager extends State
{

    @property(cc.Label) lbl_player_score: cc.Label = null;
    @property(cc.Label) lbl_enemy_score: cc.Label = null;
    @property(State) private backState: State = null;
    @property(State) private nextState: State = null;

    private scoreOfPlayer: number = 0;
    private scoreOfEnemy: number = 0;
    private isPlayerWin: boolean = true;

    scoreUpdate(scoreOfPlayer: number, scoreOfEnemy: number)
    {
        // console.log("Score Update!!!")
        this.scoreOfPlayer = scoreOfPlayer;
        this.scoreOfEnemy = scoreOfEnemy;

        this.convertString();
    }

    convertString()
    {
        this.lbl_player_score.string = this.scoreOfPlayer.toString();
        this.lbl_enemy_score.string = this.scoreOfEnemy.toString();
    }

    checkScore(mine: number, enemy: number)
    {
        if (this.scoreOfEnemy >= 5)
        {
            console.log("You Lose");
            this.isPlayerWin = false;
            this.changeState(this.nextState);
        }

        else if (this.scoreOfPlayer >= 5)
        {
            console.log("You Win");
            this.isPlayerWin = true;
            this.changeState(this.nextState);
        }

        else
        {
            this.changeState(this.backState);
        }
    }

    onStateEnter()
    {
        // console.log("ScoreManager Running...")
        this.checkScore(this.scoreOfPlayer, this.scoreOfEnemy);
    }

    onStateExit()
    {
        // console.log("ScoreManager Leaving...")
        if (this.scoreOfEnemy == 5 || this.scoreOfPlayer == 5)
        {
            this.lbl_player_score.string = "0";
            this.lbl_enemy_score.string = "0";
            GameManager.Instance.onWinnerEmit(this.isPlayerWin);
        }
    }

    start()
    {
        GameManager.Instance.node.on(GMEvent.onScoreEmit, this.scoreUpdate, this);
    }

    onDestroy()
    {
        GameManager.Instance.node.off(GMEvent.onScoreEmit, this.scoreUpdate, this);
    }
}
