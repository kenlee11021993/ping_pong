import GameManager, { GMEvent } from "./GameManager";
import Gameplay from "./Gameplay";
import State from "./StateMachine/State";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameEnd extends State
{

    @property(cc.Node) par_showResult: cc.Node = null;
    @property(cc.Label) lbl_result: cc.Label = null;

    @property(State) private nextState: State = null;

    // Result get and show
    showResult(isPlayerWin: boolean)
    {
        if (isPlayerWin)
        {
            this.lbl_result.string = "You Win :)"
            GameManager.Instance.onPlayWin();
            this.NextState();
        }
        else
        {
            this.lbl_result.string = "You Lose :("
            GameManager.Instance.onPlayLose();
            this.NextState();
        }
    }

    NextState()
    {
        this.scheduleOnce(() =>
        {
            this.changeState(this.nextState);
        }, 3)
    }

    onStateEnter()
    {
        this.par_showResult.active = true;

    }

    onStateExit()
    {
        this.par_showResult.active = false;
    }

    start()
    {
        GameManager.Instance.node.on(GMEvent.onWinnerEmit, this.showResult, this);
    }

    onDestroy()
    {
        GameManager.Instance.node.off(GMEvent.onWinnerEmit, this.showResult, this);
    }
}
