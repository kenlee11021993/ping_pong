import GameManager from "./GameManager";
import Gameplay from "./Gameplay";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BallMovement extends cc.Component
{

    // @property(cc.Node) obj_ball: cc.Node = null;
    // @property(cc.Node) spawnPoint: cc.Node = null;
    // @property(cc.Float) ball_speedX: number = 0;
    // @property(cc.Float) ball_speedY: number = 0;

    // private num_Restitution: number = 0;
    // private ballSpeed: number = 450;
    // private ballDirectionX: boolean = true;
    // private ballDirectionY: boolean = true;

    // start()
    // {
    //     // this.num_Restitution = this.obj_ball.getComponent(cc.PhysicsCollider).restitution;
    // }

    // restart()
    // {
    //     this.obj_ball.opacity = 255;
    // }

    // update(dt)
    // {
    // this.obj_ball.x += this.ball_speedX * dt;
    // this.obj_ball.y += this.ball_speedY * dt;
    // }

    onBeginContact(contact, self, other)
    {
        if (other.node.group == "enemygoal")
        {
            GameManager.Instance.onTriggerEnemyGoal();
        }

        if (other.node.group == "mygoal")
        {
            GameManager.Instance.onTriggerMyGoal();
        }

        if (other.node.group == "player")
        {
            GameManager.Instance.onTriggerPlayer();
        }

        if (other.node.group == "walltop")
        {
            GameManager.Instance.onTriggerWallTop();
        }

        if (other.node.group == "wallbtm")
        {
            GameManager.Instance.onTriggerWallBtm();
        }

    }
}
