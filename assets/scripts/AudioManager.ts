import GameManager, { GMEvent } from "./GameManager";

const { ccclass, property } = cc._decorator;

@ccclass
export default class AudioManager extends cc.Component
{

    @property(cc.AudioClip) private sound_Hit: cc.AudioClip = null;
    @property(cc.AudioClip) private sound_Goal: cc.AudioClip = null;
    @property(cc.AudioClip) private sound_Win: cc.AudioClip = null;
    @property(cc.AudioClip) private sound_Lose: cc.AudioClip = null;

    private current: any = null;

    playHit()
    {
        this.current = cc.audioEngine.play(this.sound_Hit, false, 1);
    }

    playGoal()
    {
        this.current = cc.audioEngine.play(this.sound_Goal, false, 1);
    }

    playWin()
    {
        this.current = cc.audioEngine.play(this.sound_Win, false, 1);
    }
    playLose()
    {
        this.current = cc.audioEngine.play(this.sound_Lose, false, 1);
    }

    start()
    {
        GameManager.Instance.node.on(GMEvent.onPlayHit, this.playHit, this);
        GameManager.Instance.node.on(GMEvent.onPlayGoal, this.playGoal, this);
        GameManager.Instance.node.on(GMEvent.onPlayWin, this.playWin, this);
        GameManager.Instance.node.on(GMEvent.onPlayLose, this.playLose, this);
    }

    onDestroy()
    {
        GameManager.Instance.node.off(GMEvent.onPlayHit, this.playHit, this);
        GameManager.Instance.node.off(GMEvent.onPlayGoal, this.playGoal, this);
        GameManager.Instance.node.off(GMEvent.onPlayWin, this.playWin, this);
        GameManager.Instance.node.off(GMEvent.onPlayLose, this.playLose, this);
    }
}
