const { ccclass, property } = cc._decorator;

@ccclass
export default class playerController extends cc.Component
{

    @property(cc.Node) obj_player: cc.Node = null;

    @property(cc.Float) speed: number = 0;
    @property(cc.Node) btnUp: cc.Node = null;
    @property(cc.Node) btnDown: cc.Node = null;

    public up: boolean = false;
    public down: boolean = false;

    onLoad()
    {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, this.onKeyDown, this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, this.onKeyUp, this);
    }

    start()
    {
        this.btnUp.on('touchstart', () =>
        {
            this.up = true;
        }, this.node);
        this.btnUp.on('touchend', () =>
        {
            this.up = false;
        }, this.node);

        this.btnDown.on('touchstart', () =>
        {
            this.down = true;
        }, this.node);
        this.btnDown.on('touchend', () =>
        {
            this.down = false;
        }, this.node);
    }

    onClickDown()
    {

    }

    onKeyDown(event)
    {
        switch (event.keyCode)
        {
            case cc.macro.KEY.up:
            case cc.macro.KEY.w:
            case this.btnUp:
                this.up = true;
                break;

            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
            case this.btnDown:
                this.down = true;
                break;

        }
    }

    onKeyUp(event)
    {
        switch (event.keyCode)
        {
            case cc.macro.KEY.w:
            case cc.macro.KEY.up:
            case this.btnUp:
                this.up = false;
                break;

            case cc.macro.KEY.s:
            case cc.macro.KEY.down:
            case this.btnDown:
                this.down = false;
                break;

        }
    }

    update(dt)
    {
        if (this.up)
        {
            this.obj_player.y += this.speed * dt;
        }

        if (this.down)
        {
            this.obj_player.y -= this.speed * dt;
        }
        // if (this.right)
        // {
        //     this.obj_player.x += this.speed * dt;
        // }
        // if (this.left)
        // {
        //     this.obj_player.x -= this.speed * dt;
        // }
    }

}
