
const { ccclass, property } = cc._decorator;

@ccclass
export default class foloAI extends cc.Component
{

    @property(cc.Node) private obj_enemy: cc.Node = null;
    @property(cc.Node) private targetFolo: cc.Node = null;

    update(dt)
    {
        this.obj_enemy.setPosition(cc.v3(this.obj_enemy.position.x, this.targetFolo.position.y, 0));
    }
}
