import State from "./State";

const {ccclass, property} = cc._decorator;

@ccclass
export default class StateMachine extends cc.Component {

    @property(State) private initialState: State = null;

    private currentState: State = null;

    public get CurrentState(): State { return this.currentState; };

    public changeState(newState: State) {
        if (this.currentState != null) {
            this.currentState.onStateExit();
        }

        if (newState == null) return;

        this.currentState = newState;
        this.currentState.onStateEnter();

        // cc.log(this.currentState.name);
    }

    onLoad () {
        this.node.getComponentsInChildren(State).forEach(state => {
            state.StateMachine = this;
        });
    }

    start () {
        this.changeState(this.initialState);
    }

    update (dt: number) {
        if (this.currentState != null) {
            this.currentState.onStateExecute(dt);
        }
    }

    lateUpdate () {
        if (this.currentState != null) {
            this.currentState.onStatePostExecute();
        }
    }
}
