import StateMachine from "./StateMachine";

const {ccclass, property} = cc._decorator;

@ccclass
export default abstract class State extends cc.Component {

    public onStateEnter(): void {}
    public onStateExecute(dt: number): void {}
    public onStatePostExecute(): void {}
    public onStateExit(): void {}

    private stateMachine: StateMachine = null;
    public set StateMachine(stateMachine: StateMachine) { this.stateMachine = stateMachine; };

    protected changeState(state: State) {
        this.stateMachine.changeState(state);
    }

    protected IsCurrentState(): boolean { return this.stateMachine.CurrentState == this; };

}
