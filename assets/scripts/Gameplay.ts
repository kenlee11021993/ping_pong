import GameManager, { GMEvent } from "./GameManager";
import State from "./StateMachine/State";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Gameplay extends State
{

    @property(cc.Node) private par_gameplay: cc.Node = null;
    @property(State) private nextState: State = null;

    // Player Position
    @property(cc.Node) obj_player: cc.Node = null;
    @property(cc.Node) obj_enemy: cc.Node = null;

    // Score properties
    @property(cc.Label) public lbl_player_score: cc.Label = null;
    @property(cc.Label) lbl_enemy_score: cc.Label = null;

    public scoreOfPlayer: number = 0;
    public scoreOfEnemy: number = 0;

    // Ball properties
    @property(cc.Node) obj_ball: cc.Node = null;
    @property(cc.Float) ball_speedX: number = 0;
    @property(cc.Float) ball_speedY: number = 0;

    protected numReset: number = 0;
    public ballSpeed: number = 0;
    public ballDirectionX: boolean = true;
    // public ballDirectionY: boolean = true;
    private isGameStart: boolean = false;
    // private isNewGame: boolean = false;
    private isWonRound: boolean = true;

    onGameStart()
    {
        this.scheduleOnce(() =>
        {
            let randomY = Math.floor(Math.random() * 2);
            if (randomY == 1)
            {
                // this.ballDirectionY = false;
                this.ball_speedY = -this.ball_speedY;
            } else
            {
                // this.ballDirectionY = true;
                this.ball_speedY = Math.abs(this.ball_speedY);
            }
            this.directionX();
            this.isGameStart = true;
        }, 3);
    }

    restart()
    {
        // console.log("Running restart");
        // this.checkScore(this.scoreOfPlayer, this.scoreOfEnemy);
        this.obj_ball.opacity = 0;
        this.isGameStart = false;
        this.scheduleOnce(() =>
        {
            this.obj_ball.setPosition(cc.v3(0, 0, 0));
            this.par_gameplay.active = false;
            this.changeState(this.nextState);
        }, 0.25);

    }

    //#region The ball function
    onTriggerEnemyGoal()
    {
        // console.log("EnemyGoal On Gameplay")
        this.scoreOfPlayer++;
        this.isWonRound = true;
        GameManager.Instance.onPlayGoal();
        this.restart();
    }

    onTriggerMyGoal()
    {
        // console.log("MyGoal")
        this.scoreOfEnemy++;
        this.isWonRound = false;
        GameManager.Instance.onPlayGoal();
        this.restart();
    }

    onTriggerPlayer()
    {
        // console.log("Player")
        this.ballSpeed += 15;
        GameManager.Instance.onPlayHit();
        if (this.ballDirectionX)
        {
            this.ballDirectionX = false;
            this.ball_speedX += this.ballSpeed;
            this.ball_speedX = -this.ball_speedX;
        }
        else//(!this.ballDirectionX)
        {
            this.ballDirectionX = true;
            this.ball_speedX = Math.abs(this.ball_speedX);
            this.ball_speedX += this.ballSpeed;
        }
        // this.directionX();
    }

    onTriggerWallTop()
    {
        // console.log("wall")

        // if (this.ballDirectionY)
        // {
        // console.log("W1")
        // this.ballDirectionY = false;
        GameManager.Instance.onPlayHit();
        this.ball_speedY = -this.ball_speedY;
        // }

    }

    onTriggerWallBtm()
    {
        // console.log("wall")

        // else//if (!this.ballDirectionY)
        // {
        // console.log("W2")
        // this.ballDirectionY = true;
        GameManager.Instance.onPlayHit();
        this.ball_speedY = Math.abs(this.ball_speedY);
        // GameManager.Instance
        // }
    }

    //#endregion

    directionX()
    {
        this.ballSpeed += 15;
        if (this.ballDirectionX)
        {
            console.log("to me")
            this.ballDirectionX = false;
            this.ball_speedX += this.ballSpeed;
            this.ball_speedX = -this.ball_speedX;
        }
        else//(!this.ballDirectionX)
        {
            console.log("to enemy")
            this.ballDirectionX = true;
            this.ball_speedX = Math.abs(this.ball_speedX);
            this.ball_speedX += this.ballSpeed;
        }
    }

    convertNumber()
    {
        this.scoreOfPlayer = parseInt(this.lbl_player_score.string);
        this.scoreOfEnemy = parseInt(this.lbl_enemy_score.string);
    }

    onStateExecute(dt)
    {
        if (!this.isGameStart) return;
        this.obj_ball.x += this.ball_speedX * dt;
        this.obj_ball.y += this.ball_speedY * dt;

    }

    onStateEnter()
    {
        console.log("2")
        this.par_gameplay.active = true;

        this.obj_ball.opacity = 255;
        this.ballSpeed = 0;

        if (this.isWonRound)
        {
            this.ballDirectionX = true;
        }
        else
        {
            this.ballDirectionX = false;
        }
        this.obj_player.setPosition(cc.v3(-400, 0, 0));
        this.obj_enemy.setPosition(cc.v3(400, 0, 0));

        this.onGameStart();
    }

    onNewGame()
    {
        this.numReset = this.ball_speedX;
        this.ballSpeed = 0;
        this.scoreOfPlayer = 0;
        this.scoreOfEnemy = 0;
        this.isWonRound = true;
    }

    onStateExit()
    {
        this.ball_speedX = this.numReset;
        GameManager.Instance.onScoreEmit(this.scoreOfPlayer, this.scoreOfEnemy);
    }

    start()
    {
        GameManager.Instance.node.on(GMEvent.onTriggerEnemyGoal, this.onTriggerEnemyGoal, this);
        GameManager.Instance.node.on(GMEvent.onTriggerMyGoal, this.onTriggerMyGoal, this);
        GameManager.Instance.node.on(GMEvent.onTriggerPlayer, this.onTriggerPlayer, this);
        GameManager.Instance.node.on(GMEvent.onTriggerWallTop, this.onTriggerWallTop, this);
        GameManager.Instance.node.on(GMEvent.onTriggerWallBtm, this.onTriggerWallBtm, this);
        GameManager.Instance.node.on(GMEvent.onNewGame, this.onNewGame, this);
    }

    onDestroy()
    {
        GameManager.Instance.node.off(GMEvent.onTriggerEnemyGoal, this.onTriggerEnemyGoal, this);
        GameManager.Instance.node.off(GMEvent.onTriggerMyGoal, this.onTriggerMyGoal, this);
        GameManager.Instance.node.off(GMEvent.onTriggerPlayer, this.onTriggerPlayer, this);
        GameManager.Instance.node.off(GMEvent.onTriggerWallTop, this.onTriggerWallTop, this);
        GameManager.Instance.node.off(GMEvent.onTriggerWallBtm, this.onTriggerWallBtm, this);
        GameManager.Instance.node.off(GMEvent.onNewGame, this.onNewGame, this);
    }

}
