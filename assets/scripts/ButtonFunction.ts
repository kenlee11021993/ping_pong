import view from "./view";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonFunction extends cc.Component
{
    @property(view) obj_view: view = null;


    onClickPlay()
    {
        this.obj_view.par_menu.active = false;
        this.obj_view.par_gameplay.active = true;
    }
}
