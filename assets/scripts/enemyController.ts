
const { ccclass, property } = cc._decorator;

@ccclass
export default class enemyController extends cc.Component
{

    @property(cc.Node) obj_enemy: cc.Node = null;

    @property(cc.Float) speed: number = 0;

    private up: boolean = true;
    private down: boolean = false;

    update(dt)
    {
        if (this.obj_enemy.position.y >= 270)
        {
            this.up = false;
            this.down = true;
        }

        if (this.obj_enemy.position.y <= -270)
        {
            this.down = false;
            this.up = true;
        }

        if (this.up)
        {
            this.obj_enemy.y += this.speed * dt;
        }

        if (this.down)
        {
            this.obj_enemy.y -= this.speed * dt;
        }

    }
}
