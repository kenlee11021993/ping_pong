import enemyController from "./enemyController";
import foloAI from "./foloAI";
import GameManager from "./GameManager";
import State from "./StateMachine/State";

const { ccclass, property } = cc._decorator;

@ccclass
export default class view extends State
{
    // variables
    @property(cc.Label) lbl_title: cc.Label = null;
    @property(cc.Node) par_button: cc.Node[] = [];
    @property(cc.Button) btn_play_easy: cc.Button = null;
    @property(cc.Button) btn_play_hard: cc.Button = null;
    @property(cc.Node) obj_enemy_controller: cc.Node = null;
    @property(cc.Node) par_menu: cc.Node = null;

    @property(State) private nextState: State = null;

    onClickPlayEasy()
    {
        console.log(this.obj_enemy_controller.getComponent(enemyController) + " Easy");
        this.obj_enemy_controller.getComponent(enemyController).enabled = true;
        this.changeState(this.nextState);
    }

    onClickPlayHard()
    {
        console.log(this.obj_enemy_controller.getComponent(foloAI) + " Hard");
        this.obj_enemy_controller.getComponent(foloAI).enabled = true;
        this.changeState(this.nextState);
    }

    onStateEnter()
    {
        this.obj_enemy_controller.getComponent(enemyController).enabled = false;
        this.obj_enemy_controller.getComponent(foloAI).enabled = false;
        this.par_menu.active = true;
        this.par_button.forEach(btneach =>
        {
            btneach.active = false;
        });
        // this.par_button.active = false;
        cc.tween(this.lbl_title.node)
            .to(0.6, { position: cc.v3(0, 230) })
            .delay(0.3)
            .call(() =>
            {
                this.par_button.forEach(btneach =>
                {
                    btneach.active = true;
                });
            })
            .start()
    }

    onStateExit()
    {
        let newGame = true;
        GameManager.Instance.onNewGame(newGame);
        this.par_menu.active = false;
    }

}
