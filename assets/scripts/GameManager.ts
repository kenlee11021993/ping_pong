
const { ccclass, property } = cc._decorator;

export enum GMEvent
{
    onNewGame = "onNewGame",

    onTriggerEnemyGoal = "onTriggerEnemyGoal",
    onTriggerMyGoal = "onTriggerMyGoal",
    onTriggerWallTop = "onTriggerWallTop",
    onTriggerWallBtm = "onTriggerWallBtm",
    onTriggerPlayer = "onTriggerPlayer",

    onPlayHit = "onPlayHit",
    onPlayGoal = "onPlayGoal",
    onPlayWin = "onPlayWin",
    onPlayLose = "onPlayLose",

    onScoreEmit = "onScoreEmit",

    onWinnerEmit = "onWinnerEmit",
}

@ccclass
export default class GameManager extends cc.Component
{
    private static instance: GameManager = null;
    public static get Instance(): GameManager { return this.instance; };

    //#region Play Sound
    onPlayHit()
    {
        this.node.emit(GMEvent.onPlayHit);
    }
    onPlayGoal()
    {
        this.node.emit(GMEvent.onPlayGoal);
    }
    onPlayWin()
    {
        this.node.emit(GMEvent.onPlayWin);
    }
    onPlayLose()
    {
        this.node.emit(GMEvent.onPlayLose);
    }
    //#endregion

    //#region New Game
    onNewGame(isNewGame: boolean)
    {
        this.node.emit(GMEvent.onNewGame, isNewGame);
    }
    //#endregion

    //#region Winner Event
    onWinnerEmit(isPlayerWin: boolean)
    {
        this.node.emit(GMEvent.onWinnerEmit, isPlayerWin);
    }
    //#endregion

    //#region Score Event
    onScoreEmit(scoreOfPlayer: number, scoreOfEnemy: number)
    {
        this.node.emit(GMEvent.onScoreEmit, scoreOfPlayer, scoreOfEnemy);
    }
    //#endregion

    //#region Ball Event
    onTriggerEnemyGoal()
    {
        this.node.emit(GMEvent.onTriggerEnemyGoal);
    }

    onTriggerMyGoal()
    {
        this.node.emit(GMEvent.onTriggerMyGoal);
    }

    onTriggerPlayer()
    {
        this.node.emit(GMEvent.onTriggerPlayer);
    }

    onTriggerWallTop()
    {
        this.node.emit(GMEvent.onTriggerWallTop);
    }

    onTriggerWallBtm()
    {
        this.node.emit(GMEvent.onTriggerWallBtm);
    }
    //#endregion

    onLoad()
    {
        cc.director.getPhysicsManager().enabled = true;
        cc.director.getCollisionManager().enabled = true;
        if (GameManager.instance != null)
        {
            this.node.destroy();
        }
        else
        {
            GameManager.instance = this;
        }
    }

    onDestroy()
    {
        if (GameManager.instance == this)
        {
            GameManager.instance = null;
        }
    }
}
